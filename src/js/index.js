import * as THREE from 'three';
import fragment from './fragment.glsl';
import vertex from './vertex.glsl';

/**
 * * container: див, в который пихаем канвас
 * * camera, scene, renderer - всё, как обычно
 * * uniforms - по сути шейдеры с параметрами, далее подробней
 */

let container;
let camera, scene, renderer;
let uniforms;

/**
 * * положение мыши 
 */

let mouse = {
  x: 0,
  y: 0
};

const loader = new THREE.TextureLoader();
document.onmousemove = getMouseXY;
init();
animate();

/**
 * * @param {event}
 * * функция берёт текущие значения мыши
 * * передаёт их в объект и в шейдеры
 */

function getMouseXY(e) {
  mouse.x = e.pageX;
  mouse.y = e.pageY;
  uniforms.u_mouse.value.x = mouse.x;
  uniforms.u_mouse.value.y = mouse.y;
}

function init() {
  container = document.querySelector('.app');

  camera = new THREE.Camera();
  camera.position.z = 1;
  scene = new THREE.Scene();
  const geometry = new THREE.PlaneBufferGeometry(2, 2);

  /**
   * * uniforms
   * * каждый объект - название переменной в шейдерах
   * * @param { type: 'f' } float
   * * @params { type: 'v2 } Vector2 - двумерный вектор 
   */

  uniforms = {
    u_time: {
      type: "f",
      value: 1.0
    },
    u_animation: {
      type: "f",
      value: 0.0
    },
    u_mouse: {
      type: "v2",
      value: new THREE.Vector2()
    },
    u_resolution: {
      type: "v2",
      value: new THREE.Vector2()
    },
    u_size: {
      type: "v2",
      value: new THREE.Vector2()
    }
  };

  /**
   * * Создание материала,
   * * шейдеры тащатся из хтмл
   */

  const material = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: vertex,
    fragmentShader: fragment
  });

  
  const mesh = new THREE.Mesh(geometry, material);
  scene.add(mesh);
  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);

  container.appendChild(renderer.domElement);
  onWindowResize();
  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize(event) {
  renderer.setSize(window.innerWidth, window.innerHeight);
  uniforms.u_resolution.value.x = renderer.domElement.width;
  uniforms.u_resolution.value.y = renderer.domElement.height;
  uniforms.u_mouse.value.x = mouse.x;
  uniforms.u_mouse.value.y = mouse.y;
}

function animate() {
  requestAnimationFrame(animate);
  render();
}

function render() {
  uniforms.u_time.value += 0.05;
  renderer.render(scene, camera);
}